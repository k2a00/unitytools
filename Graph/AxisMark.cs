﻿using UnityEngine;
using UnityEngine.UI;

public class AxisMark : MonoBehaviour
{
    [SerializeField] private Text valueView;
    
    public void SetMarkValue(string value)
    {
        valueView.text = value;
    }
}
