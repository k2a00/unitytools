using UnityEngine;


public class CustomKeyboard : MonoBehaviour
{
    #region Singleton
    public static CustomKeyboard Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    #endregion

    [SerializeField] private GameObject keyboardView;
    [SerializeField] private GameObject[] languagePanels;
    [SerializeField] private GameObject symbolsPanel;
    
    private CustomInputField currentInputField;
    private bool isEnglish;
    private bool isUpperCase;



    public void Show(CustomInputField field)
    {
        keyboardView.SetActive(true);

        ShowLanguageKeyboard();

        currentInputField = field;
    }

    public void Hide()
    {
        keyboardView.SetActive(false);

        currentInputField = null;
    }

    public void AddChar(string charElement)
    {
        if (currentInputField == null)
        {
            return;
        }

        if (isUpperCase)
        {
            currentInputField.AddChar(charElement.ToUpper());

            ChangeCaseValue();

            return;
        }

        currentInputField.AddChar(charElement);
    }

    public void AddSpace()
    {
        currentInputField?.AddChar(" ");
    }

    public void AddLineBreak()
    {
        currentInputField.AddChar("\n");
    }

    public void DeleteChar()
    {
        currentInputField?.DeleteChar();
    }

    public void ChangeLanguage()
    {
        isEnglish = !isEnglish;

        ShowLanguageKeyboard();
    }

    public void ChangeCaseValue()
    {
        isUpperCase = !isUpperCase;
    }

    public void MoveCaretLeft()
    {
        currentInputField.MoveCaretLeft();
    }

    public void MoveCaretRight()
    {
        currentInputField.MoveCaretRight();
    }

    public void ShowSymbolsKeyboard()
    {
        for (int i = 0; i < languagePanels.Length; i++)
        {
            languagePanels[i].SetActive(false);
        }

        isUpperCase = false;

        symbolsPanel.SetActive(true);
    }

    public void ShowLanguageKeyboard()
    {
        symbolsPanel.SetActive(false);


        if (isEnglish == true)
        {
            languagePanels[0].SetActive(true);
            languagePanels[1].SetActive(false);
        }
        else
        {
            languagePanels[0].SetActive(false);
            languagePanels[1].SetActive(true);
        }
    }
}