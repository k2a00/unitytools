using UnityEngine;

public abstract class RoomDoorwayEventsHandler : MonoBehaviour
{
    [SerializeField] protected EnterTrigger inRoomTrigger;
    [SerializeField] protected EnterTrigger outRoomTrigger;

    private void Start()
    {
        inRoomTrigger.OnTriggerEnterCallback += ProcessEnterInRoomTrigger;
        outRoomTrigger.OnTriggerEnterCallback += ProcessEnterOutRoomTrigger;
    }

    protected abstract void ProcessEnterInRoomTrigger(GameObject go);

    protected abstract void ProcessEnterOutRoomTrigger(GameObject go);
}