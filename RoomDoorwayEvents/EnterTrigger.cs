using System;
using UnityEngine;

public class EnterTrigger : MonoBehaviour
{
    public event Action<GameObject> OnTriggerEnterCallback;

    private void OnTriggerEnter(Collider other)
    {
        OnTriggerEnterCallback?.Invoke(other.gameObject);
    }
}