using UnityEngine;


public class CanvasVisibleControllerByTriggers : RoomDoorwayEventsHandler
{
    [SerializeField] private GameObject canvas;


    protected override void ProcessEnterInRoomTrigger(GameObject go)
    {
        if (go.GetComponent<PlayerMovement>())
        {
            canvas.SetActive(true);
        }
    }

    protected override void ProcessEnterOutRoomTrigger(GameObject go)
    {
        if (go.GetComponent<PlayerMovement>())
        {
            canvas.SetActive(false);
        }
    }
}