using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{
    [SerializeField] private GameObject dialogBoxView;
    [SerializeField] private GameObject rejectBtn;
    [SerializeField] private Text messageView;
    [SerializeField] private Text acceptView;
    [SerializeField] private Text rejectView;

    private event Action<bool> onResponsedCallback;


    public void Show(DialogBoxConfig config)
    {
        messageView.text = config.Message;
        acceptView.text = config.AcceptBtnText;

        onResponsedCallback = config.OnPressedBtnCallback;

        if (config.WithoutRejectBtn)
        {
            rejectBtn.SetActive(false);
        }
        else 
        {
            rejectBtn.SetActive(true);

            rejectView.text = config.RejectBtnText;
        }

        dialogBoxView.SetActive(true);
    }

    public void OnPressedAcceptBtn()
    {
        Close();

        onResponsedCallback?.Invoke(true);
    }

    public void OnPressedRejectBtn()
    {
        Close();

        onResponsedCallback?.Invoke(false);
    }

    private void Close()
    {
        dialogBoxView.SetActive(false);
    }
}
