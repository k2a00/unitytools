﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    [SerializeField] private Text timerView;
    [SerializeField] private Format format;
    
    private enum Format { WithMilliseconds, OnlySecondsWithZero, OnlySeconds };
    private readonly string[] formats = new string[3] { "{0:00.00}", "{0:00}", "{0:0}" };

    private Action methodAfterEnd;
    private string messageAfterEnd;
    private float messageShowingTime;
    private float timer = 0f;
    private bool timeShouldGo;


    private void Update()
    {
        if (!timeShouldGo)
            return;

        timer -= 1 * Time.deltaTime;
        
        if (format == Format.WithMilliseconds)
        {
            timer = Mathf.Clamp(timer, 0f, Mathf.Infinity);
        }
        
        timerView.text = string.Format(formats[(int)format], timer);

        if (timer <= 0)
        {
            timeShouldGo = false;

            if (messageAfterEnd != "")
            {
                timerView.text = messageAfterEnd;

                StartCoroutine(WaitForShowingMessage());

                return;
            }
            
            methodAfterEnd?.Invoke();
        }
    }

    public void StartTimer(float timerValue, Action _methodAfterEnd)
    {
        timer = timerValue;
        methodAfterEnd = _methodAfterEnd;
        messageAfterEnd = "";

        Unpause();
    }

    public void StartTimer(float timerValue, Action _methodAfterEnd, string _messageAfterEnd, float _messageShowingTime)
    {
        timer = timerValue;
        methodAfterEnd = _methodAfterEnd;
        messageAfterEnd = _messageAfterEnd;
        messageShowingTime = _messageShowingTime;

        Unpause();
    }

    public void Pause()
    {
        timeShouldGo = false;
    }

    public void Unpause()
    {
        timeShouldGo = true;
    }

    public void ResetTimer()
    {
        timeShouldGo = false;
        timerView.text = string.Format(formats[(int)format], 0);
    }

    public void SetTimerView(Text _timerView)
    {
        timerView = _timerView;
    }

    private IEnumerator WaitForShowingMessage()
    {
        yield return new WaitForSeconds(messageShowingTime);

        timerView.text = "";

        methodAfterEnd?.Invoke();
    }
}
