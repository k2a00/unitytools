﻿using System.Collections;
using UnityEngine;

public class DynamicInfoBoard : MonoBehaviour
{
    [SerializeField] private GameObject[] infoBlocks;
    [SerializeField] private float blockViewTime;

    private int currentIndex;
    private int blockLength;


    private void OnEnable()
    {
        currentIndex = 0;
        blockLength = infoBlocks.Length;

        StartCoroutine(ChangeBlock(new WaitForSeconds(blockViewTime)));
    }

    private IEnumerator ChangeBlock(WaitForSeconds waitForSeconds)
    {
        while(true)
        {
            yield return waitForSeconds;

            ShowNextBlock();

            if (currentIndex == blockLength - 1)
            {
                currentIndex = 0;
            }
            else
            {
                currentIndex++;
            }
        }
    }

    private void ShowNextBlock()
    {
        for (int i = 0; i < blockLength; i++)
        {
            infoBlocks[i].SetActive(false);
        }

        infoBlocks[currentIndex].SetActive(true);
    }
}
