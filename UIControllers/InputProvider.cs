using System;
using UnityEngine;
using UnityEngine.UI;

public class InputProvider : MonoBehaviour
{
    [SerializeField] private InputField inputField;
    [SerializeField] private bool needClearAfterSend;

    public event Action<string> OnPressedButton;


    public void SendInputText()
    {
        string text = inputField.text;

        if (text == "")
        {
            return;
        }

        OnPressedButton?.Invoke(text);

        if (needClearAfterSend)
        {
            inputField.text = "";
        }
    }
}
