﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float mouseSensitivity = 5f;

    private Quaternion playerNewRot;
    private Quaternion cameraNewRot;


    private void Start()
    {
        playerNewRot = playerTransform.rotation;
        cameraNewRot = cameraTransform.rotation;
    }

    private void LateUpdate()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        float speedRot = mouseSensitivity * Time.deltaTime;

        playerNewRot *= Quaternion.Euler(0, mouseX, 0);
        playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, playerNewRot, speedRot);

        cameraNewRot *= Quaternion.Euler(-mouseY, 0, 0);
        cameraTransform.localRotation = Quaternion.Lerp(cameraTransform.localRotation, cameraNewRot, speedRot);
    }
}