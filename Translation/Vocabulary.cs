﻿using UnityEngine.UI;


[System.Serializable]
public class Vocabulary
{
    public Text[] TextPlace;
    public string[] Equivalents;
}