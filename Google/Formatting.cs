using System.Collections.Generic;

public class Formatting
{
    private const char cellSeporator = ',';
    private readonly string[] endIndicator = { ",#end" };



    public string[] SplitOnLines(string rawData)
    {
        string[] lines = rawData.Split(endIndicator, System.StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < lines.Length; i++)
        {
            lines[i] = lines[i].Trim();
            
            if (lines[i].IndexOf('#') != -1)
            {
                lines[i] = lines[i].Substring(lines[i].IndexOf('#'));
            }
        }

        return lines;
    }

    public string GetLineThatStartWith(string startingWith, string[] lines)
    {
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].IndexOf(startingWith) != -1)
            {
                return lines[i];
            }
        }

        return "";
    }

    public string[] GetCellsValues(string line)
    {
        string[] cellsValues = line.Split(cellSeporator);

        for (int i = 0; i < cellsValues.Length; i++)
        {
            cellsValues[i] = cellsValues[i].Replace('~', ',');
        }

        return  cellsValues;
    }

    public int GetCellIndexThatBeginWith(string tag, string line)
    {
        string[] cellsValues = line.Split(cellSeporator);

        int neededIndex = -1;

        for (int i = 0; i < cellsValues.Length; i++)
        {
            if (cellsValues[i].StartsWith(tag))
            {
                neededIndex = i;
                break;
            }
        }
        
        return neededIndex;
    }

    public string GetSubstringAfterTag(string tag, string[] lines)
    {
        string neededLine = GetLineThatStartWith(tag, lines);

        if (neededLine == "")
        {
            return "";
        }

        int indexBegining = neededLine.IndexOf(tag) + tag.Length + 1;

        return neededLine.Substring(indexBegining).Trim();
    }

    public string GetSubstringAfterTag(string tag, string rawData)
    {
        return GetSubstringAfterTag(tag, SplitOnLines(rawData));
    }

    public string GetSubstringAfterTagWithReplacing(string tag, string[] lines)
    {
        return GetSubstringAfterTag(tag, lines).Replace('~', ',');
    }
    
    /// <summary>
    /// You should check array length. The length can be zero if no lines were found
    /// </summary>
    public string[] TryGetLinesBetweenTags(string beginTag, string endTag, string[] lines)
    {
        int lineStart = -1;
        int lineEnd = -1;

        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].IndexOf(endTag) != -1)
            {
                lineEnd = i;
                break;
            }
            else if (lines[i].IndexOf(beginTag) != -1)
            {
                lineStart = i + 1;
            }
        }

        if (lineStart == -1 || lineEnd == -1)
        {
            return  new string[0];
        }

        List<string> neededLines = new List<string>();

        int linesCount = lineEnd - lineStart;

        for (int i = 0; i < linesCount; i++)
        {
            neededLines.Add(lines[lineStart + i]);
        }

        return neededLines.ToArray();
    }
}
