﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GoogleFormSender: MonoBehaviour
{
    private string formURL;
    private event Action<bool> resultCallback;
    private List<ConfigForSendingToForm> configForSending = new List<ConfigForSendingToForm>();


    public void SendData(string url, List<ConfigForSendingToForm> configs, Action<bool> callback)
    {
        formURL = url;
        configForSending = configs;
        resultCallback = callback;

        StartCoroutine(Send());   
    }

    private IEnumerator Send()
    {
        string message = "";

        WWWForm form = new WWWForm();

        int configsCount = configForSending.Count;

        ConfigForSendingToForm[] validConfig = new ConfigForSendingToForm[configsCount];

        for (int i = 0; i < configsCount; i++)
        {
            for (int a = 0; a < configsCount; a++)
            {
                if (int.Parse(configForSending[a].CSVPos) == i)
                {
                    validConfig[i] = configForSending[a]; 
                   
                    break;
                }
            }
        }

        for (int i = 0; i < configsCount; i++)
        {
            message += $"{validConfig[i].InputText}~";
        }

        form.AddField("Message", message);
        
        UnityWebRequest www = UnityWebRequest.Post(formURL, form);

        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.DataProcessingError || www.result == UnityWebRequest.Result.ProtocolError)
        {
            resultCallback?.Invoke(false);
        }
        else
        {
            resultCallback?.Invoke(true);
        }
    }
}
